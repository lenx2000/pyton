def ask_for_number_sequence (Message):                                                  #FUNCTIE INPUT OMZETTEN NAAR LIJST
    Lijst = Message.split(".")                                                          #splits de input per punt en zet het in een lijst
    return Lijst                                                                        #stuur lijst terug


def is_valid_ip_address (numberlist):                                                   #FUNCTIE CONTROLEERT OF HET IP BESTAAT UIT 4 BYTES EN HET GETAL NIET GROTER IS DAN 255                                                                                                                       #maakt x 0,x gebruik ik om de true or false te genereren als x 0 blijft is het true als het 1 wordt is het false
    if len(numberlist)==4:                                                              #controleert of het ip bestaat uit 4 delen
        for element in numberlist:                                                      #splits de lijst per element
            if int(element) <= 255 and int(element) >= 0:                               #controleert of de waarde element niet groter is dan 255 of kleiner dan 0 is en convert element naar int
                 print("")                                                              #print niks
            else:                                                                       #als het groter dan 255 is wordt de else
                return False                                                            #ruturn false want het klopt niet er is een getal groter dan 255 is of kleiner dan 0         
    else:                                                                               #als het niet uit exact 4 bytes bestaat gebeurt dit                                                                          #controleert
        return False                                                                    #return false want het bestaat niet uit 4 bytes
    return True                                                                         #als het klopt return True


def is_valid_netmask(numberlist):                                                       #FUNCTIE DIE HET SUBNET CONTROLLEERT OF HET UIT 4 BYTES BESTAAT EN ACHTER DE 0 GEEN 1 KOMT
    binary_netmask=''                                                                   #declareer de variable met waarde niks
    if len(numberlist)==4:                                                              #controleert of het subnet bestaat uit 4 bytes
            for element in numberlist:                                                  #splits de lijst per element
                if int(element) <= 255 and int(element) >= 0:                           #controleert of het kleiner of gelijk aan 255 is en groter of gelijk aan 0
                    binary_netmask=str(binary_netmask) + bin(int(element))[2:].zfill(8)      #verander de variable van waarde met zijn waarde met daar achter het binair getal van element gevult to 8 bits

                    checking_ones = True                                                #maakt variable true dus het controlleert op 1 tjes
                    for symbol in binary_netmask:                                       #splits de variable per symbool
                        if checking_ones and int(symbol)==0:                            #als variable1 waar is en symbool is een 0 doe dit
                            checking_ones = False                                       #maakt varaible1 false dus wordt er op 0 gecheckt
                        elif not checking_ones and int(symbol)==1:                      #als variable1 false is en symbool is een 1 gebeurt dit                                                                 #
                            binary_netmask=False                                        #maakt binary_mask false
                else:                                                                   #als het groter dan 255 of kleiner dan 0 is gebeurt dit
                    binary_netmask = False                                              #maakt variable false
    else:                                                                               #anders als het niet uit exact 4 bytes bestaat gebeurt dit
        binary_netmask=False                                                            #maakt binary_netmask false
    return binary_netmask                                                               #return de waarde binary_netmask


def one_bits_in_netmask(mask):                                                          #FUNCTIE TELT AANTAL 1 VOOR DE /GETAL
    
    mask=str(mask)                                                                      #maakt variable mask de waarde van mask als een sting
    counter=0                                                                           #counter=O
    for symbol in mask:                                                                 #haal elk symbool een voor een in de loop
        if symbol =='1':                                                                #als symbool 1 is doe dit
            counter=counter+1                                                           #counter is gelijk aan zijn eigen waarde +1
        else:                                                                           #anders als het een 0 is
            counter=counter                                                             #maak counter zijn eigen waarde
    return counter                                                                      #return de waarde van counter


def apply_network_mask(host_address,netmask):                                           #FUNCTIE ZOEKT WAT HET NETWERKADDRESS IS
    nethost =['','','','']                                                              #maakt variable als een lijst uit 4 elementen
    for i in range(4):                                                                  #per keer dat de lus wordt uitgevoerdt wordt i+1 tot en met 4 dan gaat het uit de lus
        host=host_address[i]                                                            #variable is host_address element i(waarde van i)
        mask=netmask[i]                                                                 #variable is netmask element i (waarde van i)
        nethost[i]=int(host) & int(mask)                                                #zet het in de lijst (waarde van i) voert and functie uit over variable1 en 2.zet beide als integers
    return nethost                                                                      #return de waarde van nethost
        
        

def  netmask_to_wilcard_mask(netmask):                                                  #FUNCTIE ZOEKT UIT WAT DE WILDCARD IS

    wildcard_element = ""                                                               #declareer 3 varaible
    wildcard_full = ""
    wildcard=[]
    for element in netmask:                                                             #split de lijst per element en voert de lus uit
       binary_netmask=bin(int(element))[2:].zfill(8)                                    #converteer de waarde van element in binary en die waarde wordt dan de variable
       wildcard_element = ""                                                            #maak variable niks
       for bit in binary_netmask:                                                       #splits de variable per bit en voer de lus uit
           if bit == "1":                                                               #als bit 1 is gebeurt di
               wildcard_element += "0"                                                  #voeg aan de waarde van de variable een 0 toe vanachter
           else:                                                                        #anders als het een 0 is gebeurt dit
                wildcard_element += "1"                                                 #voeg aan de waarde van de variable een 1 toe vanachter
       wildcard_element = int(wildcard_element , 2)                                     #converteer de binary waarde om naar een integer
       wildcard_full=wildcard_full + "." + str(wildcard_element)                        #de variable is zichzelf+een punt en de waarde integer van die vorige variable
       wildcard=wildcard_full[1:].split(".")                                            #verwijder de eerste character van de variable en split het uit per punt zet dat in de variable
    return wildcard                                                                     #return de waarde van wildcard
        

def get_broadcast_address(network_address,wildcard_mask):                               #FUNCTIE ZOEKT UIT WAT HET BROADCAST ADRESS IS
    broadcast =['','','','']                                                            #maak een lijst uit 4 elementen met niks als waarde per element
    for i in range(4):                                                                  #voer de lus 4 keer uit en verander de waarde van i met +1
        net=network_address[i]                                                          #variable is de waarde van variable ,network_address element i(waarde van i)
        mask=wildcard_mask[i]                                                           #variable is de waarde van variable ,wildcard_mask element i(waarde van i)
        broadcast[i]=int(net) | int(mask)                                               #voer de or functie uit van beide variable en zet het in variable broadcast
    return broadcast                                                                    #return de waarde van broadcast


def prefix_length_to_max_hosts(subnet_mask):                                            #FUNCTIE BEREKENT AANTAL HOSTS
    getal_host=32-subnet_mask                                                           #doe 32 min de waarde van prefix waarde en zet het in een variable
    aantal_host=pow(2, getal_host)-2                                                    #variable is gelijk aan de macht van 2 tot de waarde van getal_host -2
    return aantal_host                                                                  #return de waarde van aantal host




Ipadress_List = ask_for_number_sequence( input('Vul een ip in '))                       #voer de fuctie uit waarbij de parameter de input is van de gebruiker. de varaible zijn waarde isgelijk aan de return waarde

Subnet_List = ask_for_number_sequence( input('Vul een subnet in '))                     #voer de functie uit waarbij de parameter de input is van de gebruiker. de varaible zijn waarde isgelijk aan de return waarde

ip_status =is_valid_ip_address(Ipadress_List)                                           #voer de functie uit en de waarde van de parameter is de waarde van variable Ipadress_List.de varaible zijn waarde isgelijk aan de return waarde

binary_netmask = is_valid_netmask(Subnet_List)                                          #voer functie uit en de waarde van de parameter is de waarde van de variable Subnet_List.de varaible zijn waarde isgelijk aan de return waarde

if not binary_netmask or not ip_status:                                                 #als het IP of subnet ongeldig is gebeurt dit
    print("IP-adres en/of subnetmasker is ongeldig.")                                   #print volgende boodschap
else:                                                                                   #anders voert dit uit
    mask_getal=one_bits_in_netmask(binary_netmask)                                      #voer functie uit met parameter als waarde van variable binary_mask.de varaible zijn waarde isgelijk aan de return waarde
    nethost=apply_network_mask(Ipadress_List,Subnet_List)                               #voer de fuctie uit waarbij de waardes van de parameters gelijk is aan Ipadress_List en Subnet_List. de varaible zijn waarde isgelijk aan de return waarde
    wildcard=netmask_to_wilcard_mask(Subnet_List)                                       #voer functie uit waarbij de waarde van de parameter gelijk is aan de variable Subnetlist.de varaible zijn waarde isgelijk aan de return waarde
    broadcast=get_broadcast_address(nethost,wildcard)                                   #voer de fuctie uit waarbij de waardes van de parameters gelijk is aan nethost en wildcard. de varaible zijn waarde isgelijk aan de return waarde
    aantal_host=prefix_length_to_max_hosts(mask_getal)                                  #voer de fuctie uit waarbij de waarde van de parameter gelijk is aan de variable mask_getal.de variable zijn waarde isgelijk aan de return waarde      

    print("IP-adres en subnetmasker zijn geldig.")                                      #zet volgende text
    print("De lengte van het subnetmasker is " + str(mask_getal) + ".")             
    print("Het adres van het subnet is " + str(nethost) + ".")
    print("Het wildcardmasker is " + str(wildcard) + ".")
    print("Het broadcastadres is " + str(broadcast) + ".")
    print("Het maximaal aantal hosts op dit subnet is " + str(aantal_host) + ".")


